# JPW - 2021 UNESC
Disciplina de Java para Web
Professor Ramon Venson

[API URL](https://unesc-pokemon-api.herokuapp.com)

Crie um fork do repositório implemente as alterações nas funções dapasta API. Proponha um merge request para as funções implementadas.

* api/aleatorio.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/aleatorio))
* api/all.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/all))
* api/balanceados.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/balanceados))
* api/fragil.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/fragil))
* api/johto_pokedex.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/johto_pokedex))
* api/kanto_pokedex.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/kanto_pokedex))
* api/lentos.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/lentos))
* api/letras.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/letras))
* api/rapidos.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/rapidos))
* api/soma_ataques_sp.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/soma_ataques_sp))
* api/soma_ataques.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/soma_ataques))
* api/soma_defesas_sp.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/soma_defesas_sp))
* api/soma_defesas.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/soma_defesas))
* api/sprites.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/sprites))
* api/tanks.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/tanks))
* api/tipo_agua.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/tipo_agua))
* api/tipo_duplo.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/tipo_duplo))
* api/tipo_fogo.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/tipo_fogo))
* api/tipo_grama.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/tipo_grama))
* api/ubers.js ([endpoint](https://unesc-pokemon-api.herokuapp.com/api/ubers))